/* eslint-disable no-magic-numbers */

const [employeesList, unitsList, warningEmployeesList] = document.querySelectorAll('.item');

fetch('mock.json')
.then(response => response.json())
.then(employees => {
    createTree(employeesList, employees);
    createUnitByPerformanceType('top', employees, unitsList);
    createUnitByPerformanceType('average', employees, unitsList);
    createUnitByPerformanceType('low', employees, unitsList);
    createWarningList(employees, warningEmployeesList);
})

function createTree(container, arr){
    container.append(createTreeDom(arr, 0));
}

function createTreeDom(arr, topPoolIdx){
    const ul = document.createElement('ul');
    const li = document.createElement('li');
    const span = document.createElement('span');
    span.textContent = arr[topPoolIdx].name;
    li.append(span);
    if (arr[topPoolIdx].pool_name){
        span.classList.add('bg');
    }
    for (let i = 0; i < arr.length; i++){
        const element = arr[i];
        if (element.rm_id === arr[topPoolIdx].id){
            const childrenUl = createTreeDom(arr, i);
            li.append(childrenUl);
        }
    }
    ul.append(li);

    return ul;
}

function createUnitByPerformanceType(performanceType, arr, container){
    const {avgSalary, elementsByPerformance} = returnUnitData(performanceType, arr);
    const p = document.createElement('p');
    p.innerHTML = `Unit by performance type ${performanceType.toUpperCase()} <br>
    (average salary: ${avgSalary}):`;
    const ul = document.createElement('ul');
    elementsByPerformance.forEach(value => {
        const li = document.createElement('li');
        li.textContent = value.name;
        ul.append(li);
    })
    container.append(p);
    container.append(ul);
}

function returnUnitData(performanceType, arr){
    const elementsByPerformance = [];
    arr.forEach(value => {
        if (value.performance === performanceType){
            elementsByPerformance.push(value);
        }
    })
    const totalSalary = elementsByPerformance.reduce((acc, curr) => acc + curr.salary, 0);
    const avgSalary = (totalSalary / elementsByPerformance.length).toFixed(2);

    return {avgSalary, elementsByPerformance}
}

function createWarningList(arr, container){
    const {avgSalary, elementsByPerformance} = returnUnitData('low', arr);
    const p = document.createElement('p');
    p.innerHTML = 'Performance is low and salary <br>is above average by pool';
    const ul = document.createElement('ul');
    elementsByPerformance.forEach(value => {
        if (value.salary < avgSalary){
            const li = document.createElement('li');
            li.textContent = value.name;
            ul.append(li);
        }
    })
    container.append(p);
    container.append(ul);
}
