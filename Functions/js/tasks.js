/* eslint-disable no-magic-numbers */

const maxElement = arr => Math.max(...arr);

const copyArray = arr => [...arr];

const addUniqueId = obj => Object.assign({}, obj, {'id': Symbol('id')});

const regroupObject = obj => {
    const {name, details: {id, age, university}} = obj;

    return {
        university,
        user: {
            age,
            firstName: name,
            id
        }
    }
}

const findUniqueElements = arr => Array.from(new Set(arr));

const hideNumber = phoneNumber => phoneNumber.slice(-4).padStart(phoneNumber.length, '*');

const required = () => {
    throw new Error('Missing property')
};
const add = (a = required(), b = required()) => a + b;

const logNames = url => {
    fetch(url).then(response => {
        if (response.ok){
            return response.json();
        }else {
            console.log(`HTTP error: ${response.status}`);
        }
    }).then(users => {
        const names = [];
        users.forEach(user => {
            names.push(user.name);
        })

        console.log(names.sort());
    }).catch(error => {
        console.log(error);
    })
}

const asyncLogNames = async (url) => {
    try{
        const response = await fetch(url);
        if (response.ok){
            const users = await response.json();
            const names = [];
            users.forEach(user => {
                names.push(user.name);
            })

            console.log(names.sort());
        }else {
            console.log(`HTTP error: ${response.status}`);
        }
    }catch (error){
        console.log(error);
    }
}
