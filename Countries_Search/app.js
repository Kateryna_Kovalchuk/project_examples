/* eslint-disable no-magic-numbers */

const appRoot = document.getElementById('app-root');

appRoot.insertAdjacentHTML('afterBegin',
    `<form>
        <h1>Countries Search</h1>
        <div class="flex-50">
          <p>Please choose type of search:</p>
          <div class="radio-buttons">
            <label class="d-block"><input type="radio" name="radio" value="byRegion">By region</label>
            <label class="d-block"><input type="radio" name="radio" value="byLanguage">By language</label>
          </div>
        </div>
        <div>
          <p class="d-inline-block">Please choose search query:</p>
          <select disabled></select>
          <select disabled class="display-none"></select>
        </div>
      </form>
      <p class="sub-content display-none">No items, please choose search query</p>`)

appRoot.insertAdjacentHTML('beforeEnd',
    `<table class="table">
        <thead class="display-none">
        <tr>
          <th>Country name <span id="arrow-country"></span></th>
          <th>Capital</th>
          <th>World Region</th>
          <th>Languages</th>
          <th>Area <span id="arrow-area"></span></th>
          <th>Flag</th>
        </tr>
        </thead>
        <tbody></tbody>
      </table>`)

const [regionsSelect, languagesSelect] = document.querySelectorAll('select');
const [regionRadioBtn, langRadioBtn] = document.querySelectorAll('input');
const regions = externalService.getRegionsList();
const languages = externalService.getLanguagesList();

fillSelect(regions, regionsSelect);
fillSelect(languages, languagesSelect);

function fillSelect(values, select) {

    let option = document.createElement('option');
    option.textContent = 'Select value';
    select.append(option)

    for (let i = 0; i < values.length; i++) {
        const v = values[i];
        option = document.createElement('option');
        option.textContent = v;
        select.append(option)
    }
}

function updateUI() {
    if (regionRadioBtn.checked) {
        regionsSelect.classList.remove('display-none');
        regionsSelect.removeAttribute('disabled');
        languagesSelect.classList.add('display-none');
    }
    if (langRadioBtn.checked) {
        languagesSelect.classList.remove('display-none');
        languagesSelect.removeAttribute('disabled');
        regionsSelect.classList.add('display-none');
    }
    this === regionRadioBtn ? languagesSelect.value = 'Select value' : regionsSelect.value = 'Select value';
}


const subContent = document.querySelector('.sub-content');
const table = document.querySelector('.table');

function hideTable() {
    if (!table.classList.contains('display-none')) {
        table.classList.add('display-none');
    }
    subContent.classList.remove('display-none');
}

function showTable() {
    if (!subContent.classList.contains('display-none')) {
        subContent.classList.add('display-none');
    }
    table.classList.remove('display-none');
}

regionRadioBtn.addEventListener('click', updateUI);
langRadioBtn.addEventListener('click', updateUI);
regionRadioBtn.addEventListener('click', hideTable);
langRadioBtn.addEventListener('click', hideTable);

regionsSelect.addEventListener('change', showTable);
languagesSelect.addEventListener('change', showTable);

function fillForm(countries) {
    const thead = document.querySelector('thead');
    const tbody = document.querySelector('tbody');
    thead.classList.remove('display-none');
    tbody.innerHTML = '';

    const row = `<tr>
            <td>{{name}}</td>
            <td>{{capital}}</td>
            <td>{{region}}</td>
            <td>{{languages}}</td>
            <td>{{area}}</td>
            <td><img src="{{flagURL}}" alt="image"></td>
          </tr>`

    for (const country of countries) {
        let rowCopy = row;
        for (const key in country) {
            if (country.hasOwnProperty(key)) {
                let value = country[key];
                if (key === 'languages' && typeof value !== 'string') {
                    value = Object.values(value).join(', ');
                }
                rowCopy = rowCopy.replace('{{' + key + '}}', value);
            }
        }
        tbody.insertAdjacentHTML('afterBegin', rowCopy);
    }
    if (tbody.innerHTML === '') {
        thead.classList.add('display-none');
    }
}

let sortByCountryNameOrder = 0;
let sortByAreaOrder = 0;
const arrowCountry = document.querySelector('#arrow-country');
const arrowArea = document.querySelector('#arrow-area');

regionsSelect.onchange = function () {
    const region = regionsSelect.value;
    const countriesByRegion = externalService.getCountryListByRegion(region);
    fillForm(countriesByRegion);
    sortByCountryNameOrder = -1;
    const sortedCountries = returnSortedData('countryName', sortByCountryNameOrder);
    fillForm(sortedCountries);
    updateArrows();
}
languagesSelect.onchange = function () {
    const language = languagesSelect.value;
    const countriesByLanguage = externalService.getCountryListByLanguage(language);
    fillForm(countriesByLanguage);
    sortByCountryNameOrder = -1;
    const sortedCountries = returnSortedData('countryName', sortByCountryNameOrder);
    fillForm(sortedCountries);
    updateArrows();
}


updateArrows();

function updateArrows() {
    arrowCountry.textContent = returnArrow(sortByCountryNameOrder);
    arrowArea.textContent = returnArrow(sortByAreaOrder);
}

function returnArrow(arrowCode) {
    const arrowUp = '🠕';
    const arrowDown = '🠗';
    const arrowDouble = '↕';

    if (arrowCode === -1) {
        return arrowUp;
    }
    if (arrowCode === 1) {
        return arrowDown;
    }

    return arrowDouble;
}

function returnTableData() {
    const tbody = document.querySelector('tbody');
    const countries = [];
    const country = {
        name: '',
        capital: '',
        region: '',
        languages: '',
        area: '',
        flagURL: ''
    };

    for (const el of tbody.children) {
        const countryCopy = Object.assign({}, country);
        let key, value;
        for (let i = 0; i < Object.keys(countryCopy).length; i++) {
            key = Object.keys(countryCopy)[i];
            if (key === 'flagURL') {
                value = el.children[i].firstElementChild.src;
            } else {
                value = el.children[i].innerHTML;
            }
            countryCopy[key] = value;
        }
        countries.push(countryCopy);
    }

    return countries;
}

function returnSortedData(sortType, sortOrder) {

    const countries = returnTableData();

    if (sortType === 'countryName' && sortOrder === 1 || sortOrder === 0) {
        countries.sort((a, b) => {
            if (a.name > b.name) {
                return 1
            }
            if (a.name < b.name) {
                return -1;
            }

            return 0;
        });
    }
    if (sortType === 'countryName' && sortOrder === -1 || sortOrder === 0) {
        countries.sort((a, b) => {
            if (a.name < b.name) {
                return 1
            }
            if (a.name > b.name) {
                return -1;
            }

            return 0;
        });
    }
    if (sortType === 'area' && sortOrder === 1) {
        countries.sort((a, b) => a.area - b.area);
    }
    if (sortType === 'area' && sortOrder === -1) {
        countries.sort((a, b) => b.area - a.area);
    }

    return countries;
}

function updateTableByCountryName() {
    sortByCountryNameOrder = sortByCountryNameOrder === -1 ? 1 : -1;
    sortByAreaOrder = 0;
    const countries = returnSortedData('countryName', sortByCountryNameOrder);
    fillForm(countries);
    updateArrows();
}

function updateTableByArea() {
    sortByAreaOrder = sortByAreaOrder === -1 ? 1 : -1;
    sortByCountryNameOrder = 0;
    const countries = returnSortedData('area', sortByAreaOrder);
    fillForm(countries);
    updateArrows();
}

arrowCountry.addEventListener('click', updateTableByCountryName);
arrowArea.addEventListener('click', updateTableByArea);
